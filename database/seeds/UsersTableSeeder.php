<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\User::create([
         'name' => 'Olee',
         'email' => 'babylee2002@gmail.com',
         'email_verified_at' => \Carbon\Carbon::now(),
         'password' =>Hash::make('password')
     ]);
    }
}
